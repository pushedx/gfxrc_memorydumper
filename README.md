# GFXRC_MemoryDumper

### About
	This project was created specifically for the "GFXRC_Generator" guide.
	Please read that guide here: https://www.elitepvpers.com/forum/sro-coding-corner/4927871-gfxrc_memorydumper.html

### Usage
	Simply run the program and type in the pid (dec/hex) of the sro_client to use and then the path of your "GFXRC_Generator" folder (without quotes)

	You can also run the program from the command line and pass in the pid (dec/hex) as an argument
		Example: GFXRC_MemoryDumper.exe 777 "C:\GFXRC_Generator"
		Example: GFXRC_MemoryDumper.exe 0x404 "C:\GFXRC_Generator"

### Source
	This is a C# .Net 5 project
	
	https://gitlab.com/pushedx/gfxrc_memorydumper

	The code is heavily commented, so a lot of additional information can be found inside.

	This project was intentionally designed and is being posted as a standalone tool.
	That means certain "common" code is directly integrated to keep things as simple as possible.

	Certain design decisions are being made with the code in order to achieve a much larger goal of advancing community development.
	When developing massive, complex projects, certain tradeoffs have to be made to ensure as much accessibility and participation is possible.
	This is just one foundational cornerstone project, so the big picture might be hard to visualize until more tools are released.

### Dependencies
	Make sure to "Restore NuGet Packages" in Visual Studio!

	Newtonsoft.Json 13.0.1
	
### FAQ
    Q.	Will the GFX_RUNTIME_CLASS structures in this project be updated?
    A.	No. This project is a standalone project simply for showing how to make use of the files "GFXRC_Generator" generates.

	Q.	What SRO version does this project target?
	A.	TRSRO 1.013
