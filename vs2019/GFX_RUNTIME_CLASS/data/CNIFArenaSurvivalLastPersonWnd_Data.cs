using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct CNIFArenaSurvivalLastPersonWnd_Data
	{
		public uint _0000; // 0x0000
		public uint _0004; // 0x0004
		public uint _0008; // 0x0008
		public uint _000C; // 0x000C
		public uint _0010; // 0x0010
	}
}
