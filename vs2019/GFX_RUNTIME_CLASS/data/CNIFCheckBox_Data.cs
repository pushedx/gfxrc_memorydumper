using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct CNIFCheckBox_Data
	{
		public uint _0000; // 0x0000
		public uint _0004; // 0x0004
	}
}
