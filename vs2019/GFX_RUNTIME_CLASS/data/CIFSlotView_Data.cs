using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct CIFSlotView_Data
	{
		public uint _0000; // 0x0000
		public uint _0004; // 0x0004
		public uint _0008; // 0x0008
		public uint _000C; // 0x000C
		public uint _0010; // 0x0010
		public uint _0014; // 0x0014
		public uint _0018; // 0x0018
		public uint _001C; // 0x001C
		public uint _0020; // 0x0020
		public uint _0024; // 0x0024
		public uint _0028; // 0x0028
		public uint _002C; // 0x002C
		public uint _0030; // 0x0030
		public uint _0034; // 0x0034
	}
}
