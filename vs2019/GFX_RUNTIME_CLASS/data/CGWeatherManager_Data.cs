using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct CGWeatherManager_Data
	{
		public uint _0000; // 0x0000
		public uint _0004; // 0x0004
		public uint _0008; // 0x0008
		public uint _000C; // 0x000C
		public uint _0010; // 0x0010
		public uint _0014; // 0x0014
		public uint _0018; // 0x0018
		public uint _001C; // 0x001C
		public uint _0020; // 0x0020
		public uint _0024; // 0x0024
		public uint _0028; // 0x0028
		public uint _002C; // 0x002C
		public uint _0030; // 0x0030
		public uint _0034; // 0x0034
		public uint _0038; // 0x0038
		public uint _003C; // 0x003C
		public uint _0040; // 0x0040
		public uint _0044; // 0x0044
		public uint _0048; // 0x0048
		public uint _004C; // 0x004C
		public uint _0050; // 0x0050
		public uint _0054; // 0x0054
		public uint _0058; // 0x0058
		public uint _005C; // 0x005C
		public uint _0060; // 0x0060
		public uint _0064; // 0x0064
		public uint _0068; // 0x0068
		public uint _006C; // 0x006C
		public uint _0070; // 0x0070
		public uint _0074; // 0x0074
		public uint _0078; // 0x0078
		public uint _007C; // 0x007C
		public uint _0080; // 0x0080
		public uint _0084; // 0x0084
		public uint _0088; // 0x0088
	}
}
