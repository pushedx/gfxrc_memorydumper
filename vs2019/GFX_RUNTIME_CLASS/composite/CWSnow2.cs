using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x84)]
	public struct CWSnow2
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CIEntity_Data IEntity; // 0x001C (0x34 bytes)
		public CWeather_Data Weather; // 0x0050 (0x20 bytes)
		public CWSnow2_Data WSnow2; // 0x0070 (0x14 bytes)
	}
}
