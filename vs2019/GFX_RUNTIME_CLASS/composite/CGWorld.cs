using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x2C)]
	public struct CGWorld
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		//public CGObj_Data Virtual_GObj; // 0x001C (0x0 bytes)
		public CGWorld_Data GWorld; // 0x001C (0x10 bytes)
	}
}
