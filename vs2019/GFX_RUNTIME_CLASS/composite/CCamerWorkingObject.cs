using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x188)]
	public struct CCamerWorkingObject
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CIEntity_Data IEntity; // 0x001C (0x34 bytes)
		public CIObject_Data IObject; // 0x0050 (0x88 bytes)
		public CCamerWorkingObject_Data CamerWorkingObject; // 0x00D8 (0xB0 bytes)
	}
}
