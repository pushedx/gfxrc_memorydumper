using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x4F4)]
	public struct CNIFTreeView
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CGWndBase_Data GWndBase; // 0x001C (0x60 bytes)
		//public CGWnd_Data Virtual_GWnd; // 0x007C (0x0 bytes)
		public CNIFWnd_Data NIFWnd; // 0x007C (0x2A8 bytes)
		public CNIFStatic_Data NIFStatic; // 0x0324 (0x20 bytes)
		public CNIFTreeView_Data NIFTreeView; // 0x0344 (0x1B0 bytes)
	}
}
