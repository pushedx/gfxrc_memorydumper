using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x4)]
	public struct CObj
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
	}
}
