using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x774)]
	public struct CNIFSafetyDevice
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CGWndBase_Data GWndBase; // 0x001C (0x60 bytes)
		//public CGWnd_Data Virtual_GWnd; // 0x007C (0x0 bytes)
		public CNIFWnd_Data NIFWnd; // 0x007C (0x2A8 bytes)
		public CNIFTileWnd_Data NIFTileWnd; // 0x0324 (0x424 bytes)
		//public CNIFFrame_Data Virtual_NIFFrame; // 0x0748 (0x0 bytes)
		public CNIFMainFrame_Data NIFMainFrame; // 0x0748 (0xC bytes)
		public CNIFSafetyDevice_Data NIFSafetyDevice; // 0x0754 (0x20 bytes)
	}
}
