using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0xF8)]
	public struct CPSLogo
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CGWndBase_Data GWndBase; // 0x001C (0x60 bytes)
		//public CGWnd_Data Virtual_GWnd; // 0x007C (0x0 bytes)
		public CProcess_Data Process; // 0x007C (0x24 bytes)
		public CPSilkroad_Data PSilkroad; // 0x00A0 (0x28 bytes)
		public CPSOuterInterface_Data PSOuterInterface; // 0x00C8 (0x24 bytes)
		public CPSLogo_Data PSLogo; // 0x00EC (0xC bytes)
	}
}
