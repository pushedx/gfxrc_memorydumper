using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x798)]
	public struct CIFNotify
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CGWndBase_Data GWndBase; // 0x001C (0x60 bytes)
		//public CGWnd_Data Virtual_GWnd; // 0x007C (0x0 bytes)
		public CIFWnd_Data IFWnd; // 0x007C (0x2CC bytes)
		public CIFStretchWnd_Data IFStretchWnd; // 0x0348 (0x438 bytes)
		public CIFNotify_Data IFNotify; // 0x0780 (0x18 bytes)
	}
}
