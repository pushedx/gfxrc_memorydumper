using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x140)]
	public struct CGEffSoundBody
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		//public CGObj_Data Virtual_GObj; // 0x001C (0x0 bytes)
		public CGEffSoundBody_Data GEffSoundBody; // 0x001C (0x124 bytes)
	}
}
