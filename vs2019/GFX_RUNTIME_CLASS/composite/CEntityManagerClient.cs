using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x3C)]
	public struct CEntityManagerClient
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CEntityManager_Data EntityManager; // 0x001C (0x20 bytes)
		//public CEntityManagerClient_Data EntityManagerClient; // 0x003C (0x0 bytes)
	}
}
