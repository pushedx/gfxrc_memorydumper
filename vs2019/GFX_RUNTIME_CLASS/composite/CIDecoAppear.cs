using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x5C)]
	public struct CIDecoAppear
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CIEntity_Data IEntity; // 0x001C (0x34 bytes)
		//public CIDeco_Data Virtual_IDeco; // 0x0050 (0x0 bytes)
		public CIDecoAppear_Data IDecoAppear; // 0x0050 (0xC bytes)
	}
}
