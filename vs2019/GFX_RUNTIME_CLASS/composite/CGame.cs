using System.Runtime.InteropServices;

namespace Native
{
	[StructLayout(LayoutKind.Sequential, Pack = 1, Size = 0x9E0)]
	public struct CGame
	{
		public CObj_Data Obj; // 0x0000 (0x4 bytes)
		public CObjChild_Data ObjChild; // 0x0004 (0x18 bytes)
		public CGFXMainFrame_Data GFXMainFrame; // 0x001C (0x464 bytes)
		public CGame_Data Game; // 0x0480 (0x560 bytes)
	}
}
