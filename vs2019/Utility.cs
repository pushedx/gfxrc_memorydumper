﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    public static class Utility
    {
        public static string GenerateLabelScript_x32dbg(Dictionary<ulong, string> labels)
        {
            var sb = new StringBuilder();
            foreach (var kvp in labels)
            {
                // We shouldn't have generated any 0 address labels, but double check
                if (kvp.Key == 0)
                    throw new Exception($"Cannot create label for '{kvp.Value}' because the address is 0!");

                sb.AppendLine($"labelset 0x{kvp.Key:X}, \"{kvp.Value}\"");
            }

            sb.AppendLine("log \"Done!\"");
            sb.AppendLine("ret");

            return sb.ToString();
        }
    }
}
