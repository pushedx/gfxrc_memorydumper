﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace Common
{
    public static class ToStringEx
    {
        private static readonly Dictionary<Type, Func<Process, object, FieldInfo, uint, StringBuilder, string, bool>>
            TypeFormatters = new();

        public static bool RegisterTypeFormatter(Type type,
            Func<Process, object, FieldInfo, uint, StringBuilder, string, bool> func)
        {
            if (TypeFormatters.ContainsKey(type))
                return false;
            TypeFormatters.Add(type, func);
            return true;
        }

        static unsafe List<T> FromFixed<T>(object value, int length)
            where T : unmanaged
        {
            var data = new List<T>();
            GCHandle hdl = GCHandle.Alloc(value, GCHandleType.Pinned);
            try
            {
                T* raw = (T*) hdl.AddrOfPinnedObject();
                for (var ix = 0; ix < length; ++ix)
                {
                    data.Add(*(raw + ix));
                }

                return data;
            }
            finally
            {
                hdl.Free();
            }
        }

        static void CommonFixedLog<T>(List<T> data, string prefix, StringBuilder sb)
            where T : unmanaged, IFormattable
        {
            sb.Append(prefix);
            var flag1 = typeof(T) == typeof(byte) || typeof(T) == typeof(sbyte);
            var flag2 = typeof(T) == typeof(ushort) || typeof(T) == typeof(short);
            var flag3 = typeof(T) == typeof(uint) || typeof(T) == typeof(int);
            var flag4 = typeof(T) == typeof(ulong) || typeof(T) == typeof(long);
            for (var i = 0; i < data.Count; i++)
            {
                if (flag1)
                    sb.AppendFormat("{0:X2} ", data[i]);
                else if (flag2)
                    sb.AppendFormat("{0:X4} ", data[i]);
                else if (flag3)
                    sb.AppendFormat("{0:X8} ", data[i]);
                else if (flag4)
                    sb.AppendFormat("{0:X16} ", data[i]);
                else
                    sb.AppendFormat("{0} ", data[i]);
                if ((i + 1) % 16 == 0)
                {
                    sb.AppendLine();
                    sb.Append(prefix);
                }
            }

            sb.AppendLine();
        }

        private static void DumpObject(Process process, Type type, object instance, StringBuilder sb, string prefix,
            ref uint offset)
        {
            foreach (var p in type.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
            {
                var recurse = false;
                sb.Append(prefix);
                if (TypeFormatters.TryGetValue(p.FieldType, out var func))
                {
                    if (!func(process, instance, p, offset, sb, prefix))
                    {
                        recurse = true;
                    }
                }
                else if (p.FieldType == typeof(IntPtr))
                {
                    sb.AppendFormat("[0x{2:X}][IntPtr] {0}: 0x{1:X}",
                        p.Name, ((IntPtr) p.GetValue(instance)).ToInt64(), offset);
                }
                else if (p.FieldType == typeof(UIntPtr))
                {
                    sb.AppendFormat("[0x{2:X}][UIntPtr] {0}: 0x{1:X}",
                        p.Name, ((UIntPtr) p.GetValue(instance)).ToUInt64(), offset);
                }
                else if (p.FieldType == typeof(byte))
                {
                    sb.AppendFormat("[0x{2:X}][byte] {0}: 0x{1:X} [{1}]",
                        p.Name, ((byte) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(sbyte))
                {
                    sb.AppendFormat("[0x{2:X}][sbyte] {0}: {1} [0x{1:X}]",
                        p.Name, ((sbyte) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(ushort))
                {
                    sb.AppendFormat("[0x{2:X}][ushort] {0}: 0x{1:X} [{1}]",
                        p.Name, ((ushort) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(short))
                {
                    sb.AppendFormat("[0x{2:X}][short] {0}: {1} [0x{1:X}]",
                        p.Name, ((short) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(int))
                {
                    sb.AppendFormat("[0x{2:X}][int] {0}: {1} [0x{1:X}]",
                        p.Name, ((int) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(uint))
                {
                    sb.AppendFormat("[0x{2:X}][uint] {0}: 0x{1:X} [{1}]",
                        p.Name, ((uint) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(long))
                {
                    sb.AppendFormat("[0x{2:X}][long] {0}: {1} [0x{1:X}]",
                        p.Name, ((long) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(ulong))
                {
                    sb.AppendFormat("[0x{2:X}][ulong] {0}: 0x{1:X} [{1}]",
                        p.Name, ((ulong) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(float))
                {
                    sb.AppendFormat("[0x{2:X}][float] {0}: {1:0.0}f",
                        p.Name, ((float) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(double))
                {
                    sb.AppendFormat("[0x{2:X}][double] {0}: {1:0.0}",
                        p.Name, ((double) p.GetValue(instance)), offset);
                }
                else if (p.FieldType == typeof(Enum))
                {
                    sb.AppendFormat("[0x{2:X}][Enum] {0}: {1}",
                        p.Name, p.GetValue(instance), offset);
                }
                else
                {
                    recurse = true;
                }

                if (!recurse)
                {
                    offset += p.FieldType.SizeOf();
                }

                if (recurse)
                {
                    if (p.CustomAttributes.Any(ca => ca.AttributeType == typeof(FixedBufferAttribute)))
                    {
                        var fixedBufferDescription = p.CustomAttributes
                            .Where(x => x.AttributeType == typeof(FixedBufferAttribute))
                            .Select(x => x.ConstructorArguments)
                            .Select(x => new KeyValuePair<Type, int>((Type) x[0].Value, (int) x[1].Value))
                            .First();

                        sb.AppendFormat("{1}:", p.FieldType.Name, p.Name);
                        sb.AppendLine();

                        if (fixedBufferDescription.Key == typeof(byte))
                        {
                            var data = FromFixed<byte>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(sbyte))
                        {
                            var data = FromFixed<sbyte>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(ushort))
                        {
                            var data = FromFixed<ushort>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(short))
                        {
                            var data = FromFixed<short>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(uint))
                        {
                            var data = FromFixed<uint>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(int))
                        {
                            var data = FromFixed<int>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(long))
                        {
                            var data = FromFixed<long>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(ulong))
                        {
                            var data = FromFixed<ulong>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(float))
                        {
                            var data = FromFixed<float>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else if (fixedBufferDescription.Key == typeof(double))
                        {
                            var data = FromFixed<double>(p.GetValue(instance), fixedBufferDescription.Value);
                            offset += ((uint) data.Count * fixedBufferDescription.Key.SizeOf());
                            CommonFixedLog(data, prefix + "\t", sb);
                        }
                        else
                        {
                            throw new Exception($"<TODO | {fixedBufferDescription.Key.Name}>");
                        }
                    }
                    else
                    {
                        sb.AppendFormat("[{0}] {1}:", p.FieldType.Name, p.Name);
                        sb.AppendLine();

                        DumpObject(process, p.FieldType, p.GetValue(instance), sb, prefix + "\t", ref offset);
                    }
                }
                else
                {
                    sb.AppendLine();
                }
            }
        }

        public static StringBuilder ToString(this Process process, string header, Type type, object instance,
            string prefix, out Exception exception)
        {
            exception = null;
            var sb = new StringBuilder();
            sb.AppendLine(header);
            try
            {
                uint offset = 0;
                DumpObject(process, type, instance, sb, prefix, ref offset);
            }
            catch (Exception ex)
            {
                exception = ex;
                sb.AppendLine(ex.ToString());
            }

            return sb;
        }
    }
}
