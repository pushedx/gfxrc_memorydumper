﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace Common
{
    public static class Memory
    {
        #region ReadBytes

        public static byte[] ReadBytes(this Process process, ulong address, ulong count)
        {
            var buffer = new byte[count];
            process.ReadBytes(address, count, buffer);
            return buffer;
        }

        public static void ReadBytes(this Process process, ulong address, ulong count, byte[] buffer)
        {
            if (!Interop.ReadProcessMemory(process.SafeHandle, (UIntPtr)address, buffer, (UIntPtr)count, out var readCount))
            {
                throw new Exception($"ReadProcessMemory failed with error {Marshal.GetLastWin32Error()}.");
            }

            if (readCount.ToUInt64() != count)
            {
                throw new Exception($"ReadProcessMemory read {readCount} / {count} bytes.");
            }
        }

        #endregion

        #region ReadArray

        public static T[] ReadArray<T>(this Process process, ulong address, ulong count) where T : struct
        {
            var buffer = process.ReadBytes(address, count * (ulong)Unsafe.SizeOf<T>());
            return buffer.ToStructArray<T>();
        }

        #endregion

        #region Read

        public static T Read<T>(this Process process, ulong address) where T : struct
        {
            return process.ReadArray<T>(address, 1)[0];
        }

        #endregion
    }
}
