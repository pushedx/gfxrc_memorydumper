﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Common;
using Native;
using Shared;

namespace GFXRC_MemoryDumper
{
    class Program
    {
        private static bool PauseOnExit { get; set; }

        static void ShowUsage()
        {
            Console.WriteLine("Usage: GFXRC_MemoryDumper.exe");
            Console.WriteLine("\tAccepts user console input for the process id/folder of GFXRC_Generator");
            Console.WriteLine("");

            Console.WriteLine("Usage: GFXRC_MemoryDumper.exe [pid] [GFXRC_Generator folder]");
            Console.WriteLine("\tUses the specified process id and GFXRC_Generator folder");
            Console.WriteLine("");
        }

        static void ValidateGFXRC(List<GFXRuntimeClass> classes)
        {
            var asm = Assembly.GetCallingAssembly();
            foreach (var gfxrc in classes)
            {
                var type = asm.GetType($"Native.{gfxrc.Name}");
                var size = type.SizeOf();
                if (size != gfxrc.TotalSize)
                {
                    throw new Exception(
                        $"The memory layout for \"{gfxrc.Name}\" is wrong: 0x{gfxrc.TotalSize:X} (expected) vs 0x{size:X} (actual)");
                }
            }
        }

        public static T DumpGlobalInstance<T>(string outputDir, Process process, uint staticInstancePtr)
            where T : struct
        {
            var instancePtr = process.Read<uint>(staticInstancePtr);

            var obj = process.Read<T>(instancePtr);
            var sb = process.ToString(
                $"[{typeof(T).Name}] 0x{instancePtr:X} [Static: 0x{staticInstancePtr:X}] (0x{typeof(T).SizeOf():X} bytes)",
                typeof(T), obj, "\t",
                out var ex);

            // Makes life easier as we start to dump more types into different folders
            Directory.CreateDirectory(outputDir);

            File.WriteAllText($"{outputDir}/{typeof(T).Name}.txt", sb.ToString());

            sb.Clear(); // Help out the GC, as we'll be dumping a lot of things and stressing it

            if (ex != null)
                throw ex;

            return obj;
        }

        static void AttachToProcess(int pid, string path)
        {
            using var process = Process.GetProcessById(pid);

            var md5 = process.HashMD5();

            // Load the correct json file for the client we attach to. We should have already run GFXRC_Generator for
            // the pid to generate the GFX_RUNTIME_CLASS that we copied into this project!
            var gfxRuntimeClasses = File.ReadAllText(Path.Combine(path, "output", md5, "GFX_RUNTIME_CLASS.json"))
                .FromJson<List<GFXRuntimeClass>>();

            // Make sure we catch any mistakes from editing class layouts
            ValidateGFXRC(gfxRuntimeClasses);

            var outputDir = Path.Combine("output", md5);
            Directory.CreateDirectory(outputDir);

            // SRO is a 15+ year old 32-bit game that doesn't have ASLR. Rather than have a bunch of
            // generic code for processing exe sections and such, we're just going to support clients
            // that follow a standard layout, where the base address is the 0x400000 and the
            // code section is at 0x401000.
            //
            // In doing so, the only clients that won't be supported, are clients that have made
            // very specific changes to break this tool. Packed clients are still technically supported
            // if the unpacking process has completed, which requires the client to be running in memory.
            // That is why this tool supports "attaching" to a process ID, rather than trying to open an
            // exe or create the process itself!
            if (process.MainModule.BaseAddress.ToInt32() != 0x400000)
                throw new Exception("The base address of the process is no 0x400000");

            var info = new StringBuilder();
            info.AppendLine($"[{process.Id}] {process.MainModule.FileName}");
            info.AppendLine($"\tImageSize: 0x{process.MainModule.ModuleMemorySize:X}");
            info.AppendLine($"\tMD5: {md5}");

            // Since we save the generated files by MD5, it becomes hard to track which folder is for what 
            // version, so we'll save a log to easily figure it out. It's ideal to rename the clients per-version
            // so that info is saved as well!
            File.WriteAllText(Path.Combine(outputDir, "GFXRC_MemoryDumper.log"), info.ToString());

            Console.WriteLine(info.ToString());

            // TODO: Make another program to find all the static address we'll need + label scripts automatically,
            // so we can just run it on any client and be good to go. For now, since we only have a few static
            // addresses to manage, and since very few people will be building projects up from this example,
            // we'll just do it by hand since sine the extra complexity would bloat the guide even more.

            Dictionary<ulong, string> x32dbg_labels = new();

            // Addresses for TRSRO 1.013 [74335A1EDD8DD81AE7EE925EA83EBA72]

            // Go to 'Constructor_GFXRuntimeClass_CGWorld'
            // Find the static address assignment with ESI
            const uint g_pCGWorld = 0x11A8848;
            x32dbg_labels.Add(g_pCGWorld, nameof(g_pCGWorld));

            // Go to 'Constructor_GFXRuntimeClass_CGFXMainFrame'
            // Scroll down and find the static address assignment with ESI (CGame)
            // Scroll down and even more and find a static address assignment with EAX (CController) after
            //      a call to Constructor_GFXRuntimeClass_CControler
            const uint g_pCGame = 0x11A86B8;
            x32dbg_labels.Add(g_pCGame, nameof(g_pCGame));

            const uint g_pCController = 0x11A86BC;
            x32dbg_labels.Add(g_pCController, nameof(g_pCController));

            // Go to 'Constructor_GFXRuntimeClass_CEntityManager'
            // Scroll down and find the static address assignment with EDI
            const uint g_pCEntityManagerClient = 0x11A86C0;
            x32dbg_labels.Add(g_pCEntityManagerClient, nameof(g_pCEntityManagerClient));

            // Go to 'Constructor_GFXRuntimeClass_CICPlayer'
            // Scroll down and find the static address assignment with EDI
            //      It's under the text "CICPlayer::CICPlayer()"
            const uint g_pCICPlayer = 0x119F020;
            x32dbg_labels.Add(g_pCICPlayer, nameof(g_pCICPlayer));

            // Go to 'Constructor_GFXRuntimeClass_CGInterface'
            // Scroll down a lot and find the static address assignment with EDI
            const uint g_pCGInterface = 0x11A8844;
            x32dbg_labels.Add(g_pCGInterface, nameof(g_pCGInterface));

            // Find the string "CGlobalDataManager::GetTIDFromObjectID ( 0x%X )"
            // Find the call before the JE above it, that's CGlobalDataManager::GetTIDFromObjectID
            // Find xregs to CGlobalDataManager::GetTIDFromObjectID to look for a global variable being set into ECX
            const uint g_GlobalDataManager = 0x1196F58;
            x32dbg_labels.Add(g_GlobalDataManager, nameof(g_GlobalDataManager));

            // Write out the x32dbg label script so we don't have to manually do it in new databases for the same client
            // We can run this later when we find interesting functions to label to save us from having to do it by hand
            if (x32dbg_labels.Any())
            {
                File.WriteAllText(Path.Combine(outputDir, "x32dbg-labelscript.txt"),
                    Utility.GenerateLabelScript_x32dbg(x32dbg_labels));
            }

            var gfxrcOutputDir = Path.Combine(outputDir, "gfxrc");

            // Now read and dump objects!
            // NOTE: This is the bare minimal dumping code that is dumping the raw layouts from GFXRC_Generator
            // I use the extra braces as scope limiters to manage local variables between the objects, as sometimes,
            // we'll need to process one object first to get data to use in another.
            // This is a testbed layout, so it'd be best to organize it differently once real work beings.

            var GWorld = DumpGlobalInstance<CGWorld>(gfxrcOutputDir, process, g_pCGWorld);
            {
            }

            var Game = DumpGlobalInstance<CGame>(gfxrcOutputDir, process, g_pCGame);
            {
            }

            var Controller = DumpGlobalInstance<CControler>(gfxrcOutputDir, process, g_pCController);
            {
            }

            var ICPlayer = DumpGlobalInstance<CICPlayer>(gfxrcOutputDir, process, g_pCICPlayer);
            {
            }

            var GInterface = DumpGlobalInstance<CGInterface>(gfxrcOutputDir, process, g_pCGInterface);
            {
            }

            var EntityManagerClient = DumpGlobalInstance<CEntityManagerClient>(gfxrcOutputDir, process, g_pCEntityManagerClient);
            {
            }
        }

        static int MainWrapper(string[] args)
        {
            if (args.Length != 0 && args.Length != 2)
            {
                ShowUsage();
                return 1; // EXIT_FAILURE
            }

            string strPid;
            string strPath;
            if (args.Length == 2)
            {
                strPid = args[0].Trim();
                strPath = args[1].Trim();
            }
            else
            {
                Console.Write("Please enter the game's process id (hex or dec): ");
                strPid = Console.ReadLine()?.Trim();

                Console.Write("Please enter GFXRC_Generator's folder (no quotes): ");
                strPath = Console.ReadLine()?.Trim();

                // When no argument is passed to the program, we assume interactive mode, so
                // pause the program on exit so the user can see if there's any issues or know
                // everything worked out.
                PauseOnExit = true;
            }

            if (string.IsNullOrEmpty(strPid) || string.IsNullOrEmpty(strPath))
            {
                ShowUsage();
                return 1; // EXIT_FAILURE
            }

            int pid;
            try
            {
                if (strPid.StartsWith("0x"))
                {
                    pid = Convert.ToInt32(strPid, 16);
                }
                else
                {
                    pid = int.Parse(strPid, NumberStyles.Integer, CultureInfo.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Unable to parse the input string [{strPid}] Reason: {ex.Message}");
            }

            // Needed to make sure we can display text in non-English languages
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            var sw = Stopwatch.StartNew();
            AttachToProcess(pid, strPath);
            sw.Stop();

            Console.WriteLine($"Successfully completed in {sw.Elapsed}");

            return 0; // EXIT_SUCCESS
        }

        static int Main(string[] args)
        {
            // NOTE: This program uses Win32 standardized return values (0 is success, non-0 is failure)
            // https://docs.microsoft.com/en-gb/dotnet/api/system.environment.exitcode?view=net-5.0

            int result;
            try
            {
                result = MainWrapper(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                result = 1; // EXIT_FAILURE
            }

            if (PauseOnExit)
            {
                Console.WriteLine("Please press Enter/Return to exit...");
                Console.ReadLine();
            }

            return result;
        }
    }
}
