﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;

namespace Common
{
    public static class Extensions
    {
        public static string HashMD5(this Process process)
        {
            using var md5 = MD5.Create();
            using var stream = File.OpenRead(process.MainModule.FileName);
            return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToUpperInvariant();
        }

        public static uint SizeOf(this Type type)
        {
            return (uint)type.SSizeOf();
        }

        public static int SSizeOf(this Type type)
        {
            if (type.ContainsGenericParameters)
                return 0;

            var methodInfo = typeof(Unsafe).GetMethod("SizeOf");
            if (methodInfo == null)
                throw new Exception($"Could not find the \"SizeOf\" method from the \"Unsafe\" type");

            var genericMethod = methodInfo.MakeGenericMethod(new[] { type });
            return (int)genericMethod.Invoke(null, null); // We want this to throw if the method is null
        }

        public static T[] ToStructArray<T>(this byte[] bytes) where T : struct
        {
            var typeSize = Unsafe.SizeOf<T>();

            if (bytes.Length % typeSize != 0)
                throw new ArgumentException("Length of bytes is not a multiple of structure size");

            var values = new T[bytes.Length / typeSize];

            if (bytes.Length > 0)
            {
                Unsafe.CopyBlockUnaligned(
                    ref Unsafe.As<T, byte>(ref values[0]), ref bytes[0],
                    (uint)bytes.Length);
            }

            return values;
        }

    }
}
